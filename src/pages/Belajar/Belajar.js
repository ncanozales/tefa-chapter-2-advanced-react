import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Search from '../../components/elements/Search/Search';

export default function Belajar() {
  const [informasi, setInformasi] = useState('Peterson');
  const [informasi2, setInformasi2] = useState('Peterson');

  return (
    <main>
      <div>
        <Search
          command={() => {
            console.log('Hello');
          }}
        />

        <h1 className='text-blue-500 text-center x1sd'>Niclos Canozales</h1>
        <div className='max-w-sm rounded overflow-hidden shadow-lg'>
          <img
            className='w-full'
            src={require('../../assets/17.jpg')}
            alt='Sunset in the mountains'
          />
          <div className='px-6 py-4'>
            <div className='font-bold text-xl mb-2'>The Coldest Sunset</div>
            <p className='text-gray-700 text-base'>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Voluptatibus quia, nulla! Maiores et perferendis eaque,
              exercitationem praesentium nihil.
            </p>
          </div>
          <div className='px-6 pt-4 pb-2'>
            <span className='inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2'>
              #photography
            </span>
            <span className='inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2'>
              #travel
            </span>
            <span className='inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2'>
              #winter
            </span>
          </div>
        </div>
        <input
          onChange={(e) => setInformasi(e.target.value)}
          defaultValue='Handoko'
          className='asd'
          type='text'
        />
        <input value={informasi2} type='text' className='asd mt-5' />

        <Link to='/'>
          <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded block m-auto mt-2'>
            Mari Kembali
          </button>
        </Link>

        <Link className='text-red-500' to='/'>
          Kembali ke Main
        </Link>
      </div>
    </main>
  );
}
