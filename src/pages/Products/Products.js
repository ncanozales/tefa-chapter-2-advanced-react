import React from 'react';
import Table from '../../components/elements/Table/Table';
import defaultData from './product.json';
import { Link, useLocation } from 'react-router-dom';
import Modal from '../../components/elements/Modal';
import Button from '../../components/elements/Button';
import { useState } from 'react';
import { useEffect } from 'react';
import Search from '../../components/elements/Search/Search';

export default function Products() {
  const today = new Date();
  const location = useLocation();

  const [data, setData] = useState(defaultData);
  const [category, setCategory] = useState('');
  const [checked, setChecked] = useState(false);
  const [search, setSearched] = useState('');

  useEffect(() => {
    if (location.state != null) {
      console.log(location.state);
      setData((x) => [location.state, ...x]);
    }
  }, []);

  useEffect(() => {
    var regex = new RegExp(search, 'g');
    search == ''
      ? setData(defaultData)
      : setData(defaultData.filter((i) => i.name.match(regex)));
  }, [search]);

  const onCheckSelect = (e) => {
    if (e.target.checked) {
      setData(data.filter((i) => Date.parse(i.expiryDate) > today));
    } else {
      category === ''
        ? setData(defaultData)
        : setData(defaultData.filter((i) => i.category == category));
    }
    setChecked((current) => !current);
  };

  const onChangeSelect = (e) => {
    setCategory(e.target.value);
    e.target.value === ''
      ? checked
        ? setData(defaultData.filter((i) => Date.parse(i.expiryDate) > today))
        : setData(defaultData)
      : checked
      ? setData(
          defaultData.filter(
            (i) =>
              i.category == e.target.value && Date.parse(i.expiryDate) > today
          )
        )
      : setData(defaultData.filter((i) => i.category == e.target.value));
  };

  const onDelete = (id) => {
    const newData = data.map((i) => {
      if (i.id === id) {
        i.isDeleted = true;
      }
      return i;
    });
    setData(newData);
  };

  const columns = [
    'Product Name',
    'Description',
    'Product Price',
    'Category',
    'Expiry Date',
    'Action',
  ];

  const rowsData = (i, idx) => {
    if (!i.isDeleted) {
      if (Date.parse(i.expiryDate) > today) {
        return (
          <>
            <tr className='p-2' key={idx}>
              <Link state={i} to={`/ProductDetail/${i.id}`}>
                <td className='p-2 flex just items-center w-72'>
                  <img
                    style={{ height: '80px', width: '80px' }}
                    className='rounded-full mr-3'
                    src={i.image}
                  />
                  {i.name}
                </td>
              </Link>

              <td className='p-2'>{i.description}</td>
              <td className='p-2 text-green-400  w-32'>
                Rp. {i.price.toLocaleString('id-ID')}
              </td>
              <td className='p-2'>{i.category}</td>
              <td className='p-2'>
                {i.expiryDate == null ? 'None' : i.expiryDate}
              </td>
              <td className='p-2 text-center' style={{ cursor: 'pointer' }}>
                <Modal command={() => onDelete(i.id)}>
                  Ingin menghapus item {i.name} ?
                </Modal>
              </td>
            </tr>
          </>
        );
      } else {
        return (
          <>
            <tr className='p-2 opacity-40' key={idx}>
              <Link to={`/ProductDetail/${i.id}`}>
                <td className='p-2 flex just items-center w-72'>
                  <img
                    style={{ height: '80px', width: '80px' }}
                    className='rounded-full mr-3'
                    src={i.image}
                  />
                  {i.name}
                </td>
              </Link>

              <td className='p-2'>{i.description}</td>
              <td className='p-2 text-green-400  w-32'>
                Rp. {i.price.toLocaleString('id-ID')}
              </td>
              <td className='p-2'>{i.category}</td>
              <td className='p-2'>
                {i.expiryDate == null ? 'None' : i.expiryDate}
              </td>
              <td className='p-2 text-center' style={{ cursor: 'pointer' }}>
                <Modal command={() => onDelete(i.id)}>
                  Ingin menghapus item {i.name} ?
                </Modal>
              </td>
            </tr>
          </>
        );
      }
    }
  };

  return (
    <div className='p-8 bg-white h-full overflow-y-auto'>
      <h4 className='mb-4 text-2xl font-bold text-[#1E293B]'>Products</h4>
      <hr className='mb-4' />

      <Search
        command={(e) => {
          setSearched(e.target.value);
        }}
      />

      <div className='flex float-right mt-5'>
        {/* Bagian Checkbox */}
        <div className='flex items-center pb-3'>
          <div className='flex items-center h-5'>
            <input
              id='helper-checkbox'
              aria-describedby='helper-checkbox-text'
              type='checkbox'
              value={checked}
              onChange={onCheckSelect}
              className='w-4 h-4 accent-indigo-500 focus:accent-indigo-500 bg-gray-100 rounded border-gray-300'
            />
          </div>
          <div className='ml-2 text-md'>
            <label
              for='helper-checkbox'
              className='font-medium text-black dark:text-black'
            >
              Hide expired products
            </label>
          </div>
        </div>

        {/* Bagian Category */}

        <div className='flex justify-center align-middle'>
          <div className='ml-7'>
            <select
              onChange={onChangeSelect}
              className='
      block
      px-3
      py-2
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-indigo-500 focus:outline-none'
              aria-label='Default select example'
            >
              <option value=''>All Category</option>
              <option value='Ready'>Ready</option>
              <option value='Pre-Order'>Pre-Order</option>
              <option value='Barang Bekas'>Barang Bekas</option>
            </select>
          </div>
        </div>

        {/* Bagian Button */}
        <Button
          dest='/Products/AddNewProduct'
          // style='btn btn__normal btn--primary ml-5'
          style='bg-indigo-500 py-2 px-6 mb-3 rounded ml-7'
          type={null}
        >
          Add New Product
        </Button>
      </div>

      <Table columns={columns} items={data} rows={rowsData} />
    </div>
  );
}
