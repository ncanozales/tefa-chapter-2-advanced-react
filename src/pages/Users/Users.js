import React from 'react';
import Table from '../../components/elements/Table';
import data from './user.json';
import Modal from '../../components/elements/Modal';

export default function Users() {
  const columns = ['Username', 'Email', 'Action'];

  const rowsData = (i, idx) => {
    return (
      <>
        <tr className='p-2' key={idx}>
          <td className='p-2  flex just items-center'>
            {' '}
            <img
              className='rounded-full mr-3'
              // src={i.profilePicture}
              src={require('../../assets/17.jpg')}
              width={70}
            />
            {i.username}
          </td>
          <td className='p-2'>{i.email}</td>
          <td className='p-2 text-center'>
            <Modal>Ingin Menghapus Username {i.username} ?</Modal>
          </td>
        </tr>
      </>
    );
  };

  return (
    <div className='p-8 bg-white h-full overflow-y-auto'>
      <h4 className='mb-4 text-2xl font-bold text-[#1E293B]'>Users</h4>
      <hr className='mb-8' />
      <Table columns={columns} items={data} rows={rowsData} />
    </div>
  );
}
