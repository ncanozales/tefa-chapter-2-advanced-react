import React from 'react';
import Form from '../../components/elements/Form/Form';
import './NewProduct.css';
import InputFIeld from '../../components/elements/InputField/InputFIeld';
import Select from '../../components/elements/Select/Select';
import TextArea from '../../components/elements/TextArea/TextArea';
import { Link, useLocation } from 'react-router-dom';
import { useState } from 'react';

export default function NewProduct() {
  const location = useLocation();
  // const currentLocation = location.pathname;
  const [productName, setProductName] = useState('No Name');
  const [productPrice, setProductPrice] = useState(0);
  const [category, setCategory] = useState('Ready');
  const [description, setDescription] = useState('No Description');
  const [dateExpiry, setDateExpiry] = useState(
    new Date().toLocaleDateString('en-CA')
  );

  const [checked, setChecked] = useState(true);
  const [baseImage, setBaseImage] = useState('');

  const uploadImage = async (e) => {
    const file = e.target.files[0];
    const base64 = await convertBase64(file);

    console.log(base64);
    setBaseImage(base64);
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };
    });
  };

  const items = [
    <div className='input-container'>
      <InputFIeld
        command={(e) => setProductName(e.target.value)}
        containerStyle='input'
        inputStyle='input__text '
        label='Product Name'
        labelStyle='input__label'
        name='product-name'
        placeholder='Enter Product Name'
        type='text'
      />
      <InputFIeld
        command={(e) => setProductPrice(e.target.value)}
        containerStyle='input'
        inputStyle='input__text '
        label='Product Price'
        labelStyle='input__label'
        name='product-price'
        placeholder='Enter Price'
        type='text'
      />
      <Select
        command={(e) => {
          setCategory(e.target.value);
        }}
        containerStyle='select'
        defaultValue='Select Category'
        label='Product Category'
        labelStyle='select__label'
        name='select-item'
        selectStyle='select__items focus:border-indigo-500'
      />
    </div>,

    <div className='split'>
      <TextArea
        command={(e) => {
          setDescription(e.target.value);
        }}
        containerStyle='textarea'
        labelStyle='textarea__label'
        label='Description'
        name='description'
        placeholder='Enter Product Description'
        textAreatStyle='textarea__text'
      />

      <div className='input__fileUpload'>
        <div
          className='image-preview'
          style={{
            backgroundImage: `url(${baseImage})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
          }}
        ></div>
        {/* <img src={baseImage} height='200px' width='200px' /> */}
        <InputFIeld
          command={(e) => uploadImage(e)}
          containerStyle='input bg-indigo-200'
          inputStyle='input__img'
          label='Upload Image'
          labelStyle='input__imglabel'
          name='product-img'
          placeholder='Upload file'
          type='file'
        />
      </div>
    </div>,
    <InputFIeld
      command={(e) => {
        if (e.target.checked) {
          setChecked(true);
        } else {
          setChecked(false);
          setDateExpiry(null);
        }
      }}
      hasExpiry={checked}
      containerStyle='checkbox accent-indigo-500 focus:accent-indigo-500 rounded'
      inputStyle='checkbox__text'
      label='Product has Expiry'
      labelStyle='checkbox__label'
      name='product-expiry'
      type='checkbox'
    />,
    <InputFIeld
      hasExpiry={checked}
      command={(e) => {
        setDateExpiry(e.target.value);
      }}
      containerStyle='input'
      inputStyle='input__date'
      label='Date Expiry'
      labelStyle='input__label'
      name='date-expiry'
      type='date'
    />,
  ];

  return (
    <main className='new-product'>
      <div className='path'>
        {/* {currentLocation.split('/').map((maping) => maping + ' / ')} */}
        <span className='text-indigo-500'>Products</span> / Add New Product
      </div>
      <hr className='devider' />
      <Form
        items={items}
        nama={productName}
        harga={productPrice}
        kategori={category}
        deskripsi={description}
        tanggal={dateExpiry}
        gambar={baseImage}
      />
    </main>
  );
}
