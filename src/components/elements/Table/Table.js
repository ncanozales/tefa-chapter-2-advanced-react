import React from 'react';
import PropTypes from 'prop-types';

export default function Table({ items, columns, rows }) {
  return (
    <>
      <table className='w-full text-gray-500 p-8 mb-6'>
        <thead className='text-white-500 uppercase bg-gray-50 dark:bg-[#e0e0e0] dark:text-gray-500'>
          <tr>
            {columns.map((i, idx) => {
              return (
                <th className='px-6 py-3 text-center' key={idx}>
                  {i}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>{items.map(rows)}</tbody>
      </table>
    </>
  );
}

Table.propTypes = {
  columns: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired,
  rows: PropTypes.func.isRequired,
};
