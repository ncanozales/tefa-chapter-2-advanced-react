import React from 'react';
import './Select.css';
import PropTypes from 'prop-types';

export default function Select({
  containerStyle,
  defaultValue,
  label,
  labelStyle,
  name,
  selectStyle,
  command,
}) {
  return (
    <div className={containerStyle}>
      <label className={labelStyle} htmlFor={name}>
        {label}
      </label>
      <select className={selectStyle} name={name} onChange={command}>
        <option value='Ready'>{defaultValue}</option>
        <option value='Ready'>Ready</option>
        <option value='Barang Bekas'>Barang Bekas</option>
        <option value='Pre-Order'>Pre-Order</option>
      </select>
    </div>
  );
}

Select.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  selectStyle: PropTypes.string.isRequired,
};
