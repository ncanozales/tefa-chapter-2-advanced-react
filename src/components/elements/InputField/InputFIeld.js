import React from 'react';
import './InputField.css';
import PropTypes from 'prop-types';
import { useState } from 'react';

export default function InputFIeld({
  containerStyle,
  inputStyle,
  label,
  labelStyle,
  name,
  placeholder,
  type,
  command,
  hasExpiry,
}) {
  if (type == 'date') {
    let today = new Date();

    if (hasExpiry) {
      return (
        <div className={containerStyle}>
          <label className={labelStyle} htmlFor={name}>
            {label}
          </label>
          <input
            defaultValue={today.toLocaleDateString('en-CA')}
            onChange={command}
            className={inputStyle}
            name={name}
            type={type}
          />
        </div>
      );
    }
    return null;
  } else if (type == 'checkbox') {
    return (
      <div className={containerStyle}>
        <label className={labelStyle} htmlFor={name}>
          {label}
        </label>
        <input
          value={hasExpiry}
          onChange={command}
          className={inputStyle}
          name={name}
          placeholder={placeholder}
          type={type}
          defaultChecked
        />
      </div>
    );
  } else if (type == 'file') {
    return (
      <div className={containerStyle}>
        <input
          onChange={command}
          className={inputStyle}
          name={name}
          placeholder={placeholder}
          type='file'
        />
      </div>
    );
  } else {
    return (
      <div className={containerStyle}>
        <label className={labelStyle} htmlFor={name}>
          {label}
        </label>
        <input
          onChange={command}
          className={inputStyle}
          name={name}
          placeholder={placeholder}
          type={type}
        />
      </div>
    );
  }
}

InputFIeld.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  inputStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  hasExpiry: PropTypes.bool.isRequired,
};
