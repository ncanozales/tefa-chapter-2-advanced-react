import React from 'react';
import './TextArea.css';
import PropTypes from 'prop-types';

export default function TextArea({
  containerStyle,
  label,
  labelStyle,
  name,
  placeholder,
  textAreatStyle,
  command,
}) {
  return (
    <div className={containerStyle}>
      <label className={labelStyle} htmlFor={name}>
        {label}
      </label>
      <textarea
        className={textAreatStyle}
        name={name}
        placeholder={placeholder}
        onChange={command}
      />
    </div>
  );
}

TextArea.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  textAreatStyleStyle: PropTypes.string.isRequired,
};
