import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Button.css';

export default function Button({
  children,
  dest,
  style,
  type,
  nama,
  harga,
  kategori,
  deskripsi,
  tanggal,
  gambar,
}) {
  if (nama != null) {
    return (
      <Link
        to={dest}
        state={{
          id: 'sku0193',
          name: nama,
          price: harga,
          category: kategori,
          description: deskripsi,
          expiryDate: tanggal,
          isDeleted: false,
          image: gambar,
        }}
      >
        <button className={style} type={type}>
          {children}
        </button>
      </Link>
    );
  } else {
    return (
      <Link to={dest}>
        <button className={style} type={type}>
          {children}
        </button>
      </Link>
    );
  }
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  dest: PropTypes.string.isRequired,
  style: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};
