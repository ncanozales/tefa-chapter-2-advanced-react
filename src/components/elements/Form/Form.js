import React from 'react';
import Button from '../Button';
import './Form.css';

export default function Form({
  items,
  nama,
  harga,
  kategori,
  deskripsi,
  tanggal,
  gambar,
}) {
  return (
    <form>
      {items.map((item) => item)}
      <div className='form__action'>
        <Button
          dest='/Products'
          style='rounded py-1.5 px-7 text-black border-2 border-indigo-600'
        >
          Cancel
        </Button>
        <Button
          dest='/Products'
          style='rounded bg-indigo-500 py-2 px-9 text-white ml-3'
          nama={nama}
          harga={harga}
          kategori={kategori}
          deskripsi={deskripsi}
          tanggal={tanggal}
          gambar={gambar}
        >
          Save
        </Button>
      </div>
    </form>
  );
}
